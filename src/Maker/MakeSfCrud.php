<?php

namespace makeplus\makeplus\Maker;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Common\Inflector\Inflector as LegacyInflector;
use Doctrine\Inflector\InflectorFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Doctrine\DoctrineHelper;
use Symfony\Bundle\MakerBundle\FileManager;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Renderer\FormTypeRenderer;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Validator\Validation;

/**
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Ryan Weaver <weaverryan@gmail.com>
 */
final class MakeSfCrud extends AbstractMaker
{
    private $doctrineHelper;

    private $formTypeRenderer;

    private $inflector;

    private $base_namespace;

    private $fileManager;

    public function __construct(DoctrineHelper $doctrineHelper,
                                FormTypeRenderer $formTypeRenderer,
                                string $base_namespace,
                                FileManager $fileManager)
    {
        $this->base_namespace = $base_namespace;
        $this->doctrineHelper = $doctrineHelper;
        $this->formTypeRenderer = $formTypeRenderer;
        $this->fileManager = $fileManager;

        if (class_exists(InflectorFactory::class)) {
            $this->inflector = InflectorFactory::create()->build();
        }
    }

    public static function getCommandName(): string
    {
        return 'sf:Crud';
    }

    public static function getCommandDescription(): string
    {
        return 'Creates CRUD plus for Doctrine entity class';
    }

    /**
     * {@inheritdoc}
     */
    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $command
            ->setDescription('CRUD chuleado :)')
            ->addArgument('grupo',
                            InputArgument::REQUIRED,
                            'Grupo al que pertenece el crud'
                        )
            ->addArgument('controller-name',
                            InputArgument::REQUIRED,
                            'Nombre del controller a crear para el CRUD'
                        )
            ->addArgument('entity-class',
                            InputArgument::REQUIRED,
                            'Entity para el crud'
                        )
            ->addArgument('theme',
                            InputArgument::REQUIRED,
                            'Templates para el crud',
                        )
            ->addArgument('route-base',
                            InputArgument::REQUIRED,
                            'Nombre raiz de los route | (d)efault (c)ustom '
                        )
        ;

        $inputConfig->setArgumentAsNonInteractive('grupo');
        $inputConfig->setArgumentAsNonInteractive('entity-class');
        $inputConfig->setArgumentAsNonInteractive('controller-name');
        $inputConfig->setArgumentAsNonInteractive('theme');
        $inputConfig->setArgumentAsNonInteractive('route-base');
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
        if (null === $input->getArgument('grupo')) {
            $argument = $command->getDefinition()->getArgument('grupo');
            $question = new Question($argument->getDescription());
            $value = $io->askQuestion($question);
            $input->setArgument('grupo', $value);
        }

        if (null === $input->getArgument('controller-name')) {
            $argument = $command->getDefinition()->getArgument('controller-name');
            $question = new Question($argument->getDescription());
            $value = $io->askQuestion($question);
            $input->setArgument('controller-name', $value);
        }

        if (null === $input->getArgument('entity-class')) {
            $argument = $command->getDefinition()->getArgument('entity-class');
            $entities = $this->doctrineHelper->getEntitiesForAutocomplete();
            /*
            $question = new Question($argument->getDescription());
            $question->setAutocompleterValues($entities);
            */
            $themes = ['base', 'admin'];
            $question = new ChoiceQuestion($argument->getDescription(),
                                            $entities);

            $value = $io->askQuestion($question);
            $input->setArgument('entity-class', $value);
        }

        if (null === $input->getArgument('theme')) {
            $argument = $command->getDefinition()->getArgument('theme');
            $themes = ['base', 'admin'];
            $question = new ChoiceQuestion($argument->getDescription(),
                                            $themes, 0);

            $value = $io->askQuestion($question);
            $input->setArgument('theme', $value);
        }

        if (null === $input->getArgument('route-base')) {
            $argument = $command->getDefinition()->getArgument('route-base');
            $options = ['d', 'c'];
            $question = new ChoiceQuestion($argument->getDescription(),
                                            $options, 0);
            $value = $io->askQuestion($question);

            if ($value == 'c') {
                $question = new Question('Ingresar nombre');
                $value = $io->askQuestion($question);
            } else {
                $value = $input->getArgument('grupo').'/'.$input->getArgument('controller-name').'/crud';
            }

            $input->setArgument('route-base', $value);
        } elseif ('default' == $input->getArgument('route-base')) {
            $value = $input->getArgument('grupo').'/'.$input->getArgument('controller-name').'/crud';
            $input->setArgument('route-base', $value);
        }
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $io->title('Configuracion');

        $io->listing([
            'grupo:           '.$input->getArgument('grupo'),
            'controller-name: '.$input->getArgument('controller-name'),
            'entity-class:    '.$input->getArgument('entity-class'),
            'theme:           '.$input->getArgument('theme'),
            'route-base:      '.$input->getArgument('route-base'),
        ]);

        $io->title('Loading Data');

        $io->section('Entitys');

        $entityClassDetails = $generator->createClassNameDetails(
            Validator::entityExists(
                $input->getArgument('entity-class'),
                $this->doctrineHelper->getEntitiesForAutocomplete()
            ),
            'Entity\\'
        );

        $entityDoctrineDetails = $this->doctrineHelper->createDoctrineDetails($entityClassDetails->getFullName());

        $io->note($entityClassDetails->getFullName());

        $io->section('Repository');

        $repositoryVars = [];

        if (null !== $entityDoctrineDetails->getRepositoryClass()) {
            $repositoryClassDetails = $generator->createClassNameDetails(
                '\\'.$entityDoctrineDetails->getRepositoryClass(),
                'Repository\\',
                'Repository'
            );

            $repositoryVars = [
                'repository_full_class_name' => $repositoryClassDetails->getFullName(),
                'repository_class_name' => $repositoryClassDetails->getShortName(),
                'repository_var' => lcfirst($this->singularize($repositoryClassDetails->getShortName())),
            ];
            $io->note($repositoryClassDetails->getFullName());
            dump($repositoryVars);
        } else {
            $io->getErrorStyle()->warning('No se encontro el repocitorio.');
        }

        $io->section('Controller');

        $controllerName = $input->getArgument('controller-name') != null ? $input->getArgument('controller-name') : $entityClassDetails->getRelativeNameWithoutSuffix();

        dump($controllerName);
        $controllerClassDetails = $this->createClassNameDetails(
            $controllerName.'Controller',
            'Controller\\'.($input->getArgument('grupo') ? $input->getArgument('grupo').'\\' : ''),
            'Controller'
        );

        $io->note($controllerClassDetails->getFullName());

        $io->section('Forms');

        $baseFormName = ($input->getArgument('controller-name') != null ?
                            $input->getArgument('controller-name') :
                            $entityClassDetails->getRelativeNameWithoutSuffix());

        $io->note('BASE: '.$baseFormName);

        $formCreatedClassDetails = $this->_initForms($baseFormName, 'Created', $input->getArgument('grupo'));
        $io->note('Created: '.$formCreatedClassDetails->getFullName());

        $io->section('FormEdit');
        $formEditClassDetails = $this->_initForms($baseFormName, 'Edit', $input->getArgument('grupo'));
        $io->note('Edit:    '.$formEditClassDetails->getFullName());

        $io->section('FormFilter');
        $formFilterClassDetails = $this->_initForms($baseFormName, 'Filter', $input->getArgument('grupo'));
        $io->note('Filter:  '.$formFilterClassDetails->getFullName());

        $io->section('Routing');

        $entityVarPlural = lcfirst($this->pluralize($entityClassDetails->getShortName()));
        $entityVarSingular = lcfirst($this->singularize($entityClassDetails->getShortName()));

        $entityTwigVarPlural = Str::asTwigVariable($entityVarPlural);
        $entityTwigVarSingular = Str::asTwigVariable($entityVarSingular);

        $routeName = Str::asRouteName(($input->getArgument('grupo') ? $input->getArgument('grupo').'_' : '').$controllerClassDetails->getRelativeNameWithoutSuffix());
        $templatesPath = Str::asFilePath(($input->getArgument('grupo') ? $input->getArgument('grupo').'/' : '').$controllerClassDetails->getRelativeNameWithoutSuffix());

        $exist_controllerClassDetails = $this->fileExisteConfirm($io, 'El Controlador ya existe', $controllerClassDetails);

        $exist_formCreatedClassDetails = $this->fileExisteConfirm($io, 'El formulario Created ya existe', $formCreatedClassDetails);
        $exist_formEditClassDetails = $this->fileExisteConfirm($io, 'El formulario Edit ya existe', $formEditClassDetails);
        $exist_formFilterClassDetails = $this->fileExisteConfirm($io, 'El formulario Filter ya existe', $formFilterClassDetails);

        dump('entityVarPlural:       '.$entityVarPlural);
        dump('entityVarSingular:     '.$entityVarSingular);
        dump('entityTwigVarPlural:   '.$entityTwigVarPlural);
        dump('entityTwigVarSingular: '.$entityTwigVarSingular);
        dump('routeName:             '.$routeName);
        dump('templatesPath:         '.$templatesPath);

        dump('ResourcePath:          '.
        $this->getResourceFile(
            $input->getArgument('theme'),
            'controller',
            'Controller.tpl.php'
        )
    );

        if (!$io->confirm('Quiere ejecutar el generador', true)) {
            dd('Salida');
        }

        $io->title('Generador');

        if (!$exist_controllerClassDetails) {
            $io->note('Controller generado');
            $generator->generateController(
                $controllerClassDetails->getFullName(),
                $this->getResourceFile($input->getArgument('theme'), 'controller', 'Controller.tpl.php'),
                array_merge([
                        'entity_full_class_name' => $entityClassDetails->getFullName(),
                        'entity_class_name' => $entityClassDetails->getShortName(),
                        'form_full_class_name' => $formCreatedClassDetails->getFullName(),
                        'form_class_name' => $formCreatedClassDetails->getShortName(),

                        'form_create_full_class_name' => $formCreatedClassDetails->getFullName(),
                        'form_create_class_name' => $formCreatedClassDetails->getShortName(),

                        'form_edit_full_class_name' => $formEditClassDetails->getFullName(),
                        'form_edit_class_name' => $formEditClassDetails->getShortName(),

                        'form_filter_full_class_name' => $formFilterClassDetails->getFullName(),
                        'form_filter_class_name' => $formFilterClassDetails->getShortName(),

                        'route_path' => Str::asRoutePath(($input->getArgument('grupo') ? strtoupper(substr($input->getArgument('grupo'), 0, 1)).substr($input->getArgument('grupo'), 1) : '').$controllerClassDetails->getRelativeNameWithoutSuffix()),
                        'route_name' => $routeName,
                        'templates_path' => $templatesPath,
                        'entity_var_plural' => $entityVarPlural,
                        'entity_twig_var_plural' => $entityTwigVarPlural,
                        'entity_var_singular' => $entityVarSingular,
                        'entity_twig_var_singular' => $entityTwigVarSingular,
                        'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                    ],
                    $repositoryVars
                )
            );
        } else {
            $io->note('Controller no generado');
        }

        if (!$exist_formCreatedClassDetails) {
            $io->note('Form Created generado');
            $this->formTypeRenderer->render($formCreatedClassDetails, $entityDoctrineDetails->getFormFields(), $entityClassDetails);
        } else {
            $io->note('Form Created no generado');
        }

        if (!$exist_formEditClassDetails) {
            $io->note('Form Edit generado');
            $this->formTypeRenderer->render($formEditClassDetails, $entityDoctrineDetails->getFormFields(), $entityClassDetails);
        } else {
            $io->note('Form Edit no generado');
        }

        if (!$exist_formFilterClassDetails) {
            $io->note('Form Filter generado');
            $this->formTypeRenderer->render($formFilterClassDetails, [], $entityClassDetails);
        } else {
            $io->note('Form Filter no generado');
        }

        $templates = [];

        $templates['base'] = [
            '_delete_form' => [
                'route_name' => $routeName,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
            ],
            '_form_create' => [],
            '_form_edit' => [],
            '_form_filters' => [],
            'edit' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            'list' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_plural' => $entityTwigVarPlural,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            'new' => [
                'title' => $entityClassDetails->getShortName(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            'show' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
        ];

        $templates['admin'] = [
            '_delete_form' => [
                'route_name' => $routeName,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
            ],
            '_form_create' => [],
            '_form_edit' => [],
            '_form_filters' => [],
            'edit' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            '_edit_header' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            'list' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_plural' => $entityTwigVarPlural,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            '_list_header' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_plural' => $entityTwigVarPlural,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            '_list_table' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_plural' => $entityTwigVarPlural,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],

            'new' => [
                'title' => $entityClassDetails->getShortName(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            'show' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
            '_show_header' => [
                'title' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
                'root_template_views' => $templatesPath,
            ],
        ];

        foreach ($templates[$input->getArgument('theme')] as $template => $variables) {
            $file = $this->fileManager->getPathForTemplate($templatesPath.'/'.$template.'.html.twig');
            $afile = $this->fileManager->absolutizePath($file);

            if (is_readable($afile)) {
                $io->title('El archivo '.$file.' ya esiste');
                if ($io->confirm('¿ Quiere sobreescribirlo ?', true)) {
                    unlink($afile);
                } else {
                    continue;
                }
            }

            $generator->generateTemplate(
                $templatesPath.'/'.$template.'.html.twig',
                $this->getSkeletonPath($input->getArgument('theme'), 'templates').$template.'.tpl.php',
                $variables
            );
        }

        $generator->writeChanges();

        $this->writeSuccessMessage($io);

        $io->text(sprintf('Next: Check your new CRUD by going to <fg=yellow>%s/</>', Str::asRoutePath($controllerClassDetails->getRelativeNameWithoutSuffix())));
    }

    public function fileExisteConfirm($io, $title, $class)
    {
        if (class_exists($class->getFullName())) {
            $io->title($title);
            if ($io->confirm('¿ Quiere sobreescribirlo ?', true)) {
                $file = $this->fileManager->getRelativePathForFutureClass($class->getFullName());
                if (is_readable($file)) {
                    unlink($file);

                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function configureDependencies(DependencyBuilder $dependencies)
    {
        $dependencies->addClassDependency(
            Route::class,
            'router'
        );

        $dependencies->addClassDependency(
            AbstractType::class,
            'form'
        );

        $dependencies->addClassDependency(
            Validation::class,
            'validator'
        );

        $dependencies->addClassDependency(
            TwigBundle::class,
            'twig-bundle'
        );

        $dependencies->addClassDependency(
            DoctrineBundle::class,
            'orm-pack'
        );

        $dependencies->addClassDependency(
            CsrfTokenManager::class,
            'security-csrf'
        );

        $dependencies->addClassDependency(
            ParamConverter::class,
            'annotations'
        );
    }

    private function pluralize(string $word): string
    {
        if (null !== $this->inflector) {
            return $this->inflector->pluralize($word);
        }

        return LegacyInflector::pluralize($word);
    }

    private function singularize(string $word): string
    {
        if (null !== $this->inflector) {
            return $this->inflector->singularize($word);
        }

        return LegacyInflector::singularize($word);
    }

    public function getSkeletonPath($theme, $dir_elemente = null)
    {
        $path = dirname(__DIR__).'/Resources/skeleton/crud/'.$theme.'/'.($dir_elemente ? $dir_elemente.'/' : '');
        if (!is_dir($path)) {
            dd('El directorio del skeleton no existe '.$path);
        }

        return $path;
    }

    public function getResourceFile(string $theme, string $dir_elemente, string $file)
    {
        $path = $this->getSkeletonPath($theme, $dir_elemente);

        $file = $path.$file;
        if (!is_readable($file)) {
            dd('El archivo del skeleton no existe '.$file);
        }

        return $file;
    }

    private function _initForms(string $module, string $formNameCreated, $grupo): ClassNameDetails
    {
        return $this->createClassNameDetails(
            $formNameCreated.'Type',
            'Form\\'.($grupo ? $grupo.'\\' : '').$module.'\\',
            'Type'
        );

        $iter = 0;
        do {
            $formClassDetails = $this->createClassNameDetails(
                $formNameCreated.($iter ?: '').'Type',
                'Form\\'.($grupo ? $grupo.'\\' : '').$module.'\\',
                'Type'
            );
            ++$iter;
        } while (class_exists($formClassDetails->getFullName()));

        return $formClassDetails;
    }

    private function createClassNameDetails(string $name,
                                            string $namespacePrefix,
                                            string $suffix = '',
                                            string $validationErrorMessage = ''): ClassNameDetails
    {
        $fullNamespacePrefix = $this->base_namespace.'\\'.$namespacePrefix;
        if ('\\' === $name[0]) {
            // class is already "absolute" - leave it alone (but strip opening \)
            $className = substr($name, 1);
        } else {
            $className = rtrim($fullNamespacePrefix, '\\').'\\'.Str::asClassName($name, $suffix);
        }

        Validator::validateClassName($className, $validationErrorMessage);

        // if this is a custom class, we may be completely different than the namespace prefix
        // the best way can do, is find the PSR4 prefix and use that
        if (0 !== strpos($className, $fullNamespacePrefix)) {
            dd('asd');
            $fullNamespacePrefix = $this->fileManager->getNamespacePrefixForClass($className);
        }

        return new ClassNameDetails($className, $fullNamespacePrefix, $suffix);
    }
}
