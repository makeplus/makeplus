<?php

namespace makeplus\makeplus\DependencyInjection; 

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class MakeplusExtension extends Extension {

    public function load(array $configs, ContainerBuilder $container)
    {
        $confPath = '/Resources/config';
        $confDir = dirname(__DIR__) . $confPath;

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__. $confPath )
        );
        $loader->load( $confDir . '/services/sf_crud.yaml', 'glob');
    }

}