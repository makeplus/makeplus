<?php

namespace makeplus\makeplus\Controller;

use Symfony\Component\HttpFoundation\Request;

class sfCrudAdminAbstractController extends sfCrudAdminVersionAbstractController
{
    public $rows_per_page = 20;

    /**
     * @see sfCrudAdminAbstractController::changeFilters
     *
     * @param Request $request
     * @param string  $name    Nombre de la variable por GET
     */
    public function changeFilters($__deprecated = null, $name)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $filter = $request->request->all($name);
        if (isset($filter['reset'])) {
            $request->getSession()->remove($this->nameSessionFilter);

            return;
        } else {
            if ($request->request->has($name)) {
                $data = $request->request->all($name);
                $request->getSession()->set($this->nameSessionFilter, $data);
            }
        }
    }

    /**
     * @see sfCrudAdminAbstractController::applyFilters
     *
     * @param $formFilter
     *
     * @return $formFilter
     */
    public function applyFilters(Request $request, $formFilter)
    {
        $data = $request->getSession()->get($this->nameSessionFilter);
        if (is_array($data) and count($data)) {
            $formFilter->submit($data);
        }

        return $formFilter;
    }

    /**
     * @see sfCrudAdminAbstractController::applyFilterUpdate
     *
     * @param Request $request
     * @param $formFilter
     * @param $filterBuilderUpdater
     * @param $qb
     *
     * @return
     */
    public function applyFilterUpdate($formFilter, $filterBuilderUpdater, $qb)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $data = $request->getSession()->get($this->nameSessionFilter);
        if (is_array($data) and count($data)) {
            $formFilter->submit($data);
            $filterBuilderUpdater->addFilterConditions($formFilter, $qb);
        }

        return $formFilter;
    }

    /**
     * @see sfCrudAdminAbstractController::crudNew
     *
     * @param $obj
     * @param $form_class
     * @param $request
     * @param $redirect_after_save = null
     * @param $redirect_after_save_params = []
     *
     * @return
     */
    public function crudNew($obj, $form_class, $request, $redirect_after_save = null, $redirect_after_save_params = [])
    {
        $form = $this->createForm($form_class, $obj);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveNew($obj, $form);

            return $this->redirectToRoute(($redirect_after_save) ? $redirect_after_save : $this->route_prefix.'list', $redirect_after_save_params);
        }

        return $this->renderCrud('new.html.twig', [$this->nameVarToTheme => $obj, 'form' => $form->createView()]);
    }

    /**
     * @see sfCrudAdminAbstractController::saveNew
     *
     * @param $obj
     * @param $form
     *
     * @return
     */
    public function saveNew($obj, $form)
    {
        $this->__persistAndFlush($obj);
    }

    /**
     * @see sfCrudAdminAbstractController::crudEdit
     *
     * @param $obj
     * @param $form_class
     * @param $request
     * @param $redirect_after_save = null
     * @param $redirect_after_save_params = []
     *
     * @return
     */
    public function crudEdit($obj, $form_class, $request, $redirect_after_save = null, $redirect_after_save_params = [])
    {
        $form = $this->createForm($form_class, $obj);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveEdit($obj, $form);

            return $this->redirectToRoute(($redirect_after_save) ? $redirect_after_save : $this->route_prefix.'list', $redirect_after_save_params);
        }

        return $this->renderCrud('edit.html.twig', [$this->nameVarToTheme => $obj, 'form' => $form->createView()]);
    }

    /**
     * @see sfCrudAdminAbstractController::saveEdit
     *
     * @param $obj
     * @param $form
     *
     * @return
     */
    public function saveEdit($obj, $form)
    {
        $this->__persistAndFlush($obj);
    }

    /**
     * @see sfCrudAdminAbstractController::renderCrud
     *
     * @return
     */
    public function renderCrud(string $theme, array $vars)
    {
        return $this->render($this->templates_path.'/'.$theme, $vars);
    }

    private function __persistAndFlush($obj)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($obj);
        $entityManager->flush();
    }
}
