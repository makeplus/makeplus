<?php

namespace makeplus\makeplus\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Kernel;

if (Kernel::MAJOR_VERSION >= 6) {
    class sfCrudAdminVersionAbstractController extends AbstractController
    {
        public static function getSubscribedServices(): array
        {
            return array_merge(parent::getSubscribedServices(), [
                'doctrine' => '?'.ManagerRegistry::class,
            ]);
        }

        protected function getDoctrine(): ManagerRegistry
        {
            if (!$this->container->has('doctrine')) {
                throw new \LogicException('The DoctrineBundle is not registered in your application. Try running "composer require symfony/orm-pack".');
            }

            return $this->container->get('doctrine');
        }
    }
} else {
    class sfCrudAdminVersionAbstractController extends AbstractController
    {
    }
}
