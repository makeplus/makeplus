<?= $helper->getHeadPrintCode($title); ?>

{% block body %}
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
  <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-1">
      <div class="d-flex align-items-baseline mr-5">
        <h5 class="text-dark font-weight-bold my-2 mr-5"><?php echo $title ?></h5>
          {#
            {% if form_filter %}
          <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
          <a data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters" class="btn btn-light-primary font-weight-bolder btn-sm">Filtrar</a>
          {% endif %}
          #}
      </div>
    </div>
    <div class="d-flex align-items-center">
      <a class="btn btn-success" href="{{ path('<?= $route_name ?>_new') }}">Nuevo</a>
    </div>
  </div>
</div>
<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card">
      <div class="card-body">
        <div id="row-crud-filter" class="row">
          {#
          {% if form_filter %}
          {{ include('<?php echo $root_template_views ?>/_form_filters.html.twig', {'button_label': 'Buscar'}) }}
          {% endif %}
          #}
        </div>
        <div id="row-crud-header" class="row">
        </div>
        <div id="row-crud-table" class="row">
          <table class="table">
            <thead>
              <tr>
<?php foreach ($entity_fields as $field): ?>
                  <th><?php echo ucfirst($field['fieldName']) ?></th>
<?php endforeach; ?>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {% for <?= $entity_twig_var_singular ?> in <?= $entity_twig_var_plural ?> %}
              <tr>
<?php foreach ($entity_fields as $field): ?>
                  <td>{{ <?php echo $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
<?php endforeach; ?>
                <td>
                  <a class="btn btn-show btn-info" href="{{ path('<?= $route_name ?>_show', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}">Ver</a>
                  <a class="btn btn-edit btn-warning" href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}">Editar</a>
                </td>
              </tr>
              {% else %}
              <tr>
                <td colspan="<?= (count($entity_fields) + 1) ?>">no records found</td>
              </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
        <div id="row-crud-foot" class="row">
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
