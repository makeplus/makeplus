<?php echo $helper->getHeadPrintCode('Vista '.$title .' #{{ '. $entity_twig_var_singular .'.'. $entity_identifier.' }}') ?>

{% block body %}


<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
  <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-1">
      <div class="d-flex align-items-baseline mr-5">
        <h5 class="text-dark font-weight-bold my-2 mr-5"><?php echo $title ?></h5>
      </div>
    </div>
    <div class="d-flex align-items-center">
        <a class="btn btn-info" href="{{ path('<?= $route_name ?>_list') }}">Volver</a>
        <a class="btn btn-warning" href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}">Editar</a>
        {{ include('<?= $root_template_views ?>/_delete_form.html.twig') }}
    </div>
  </div>
</div>

<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card">
      
      <div class="card-body">

        <div class="row">
          <div class="col-12">
            <table class="table">
                <tbody>
<?php foreach ($entity_fields as $field): ?>
                        <tr>
                            <th><?= ucfirst($field['fieldName']) ?></th>
                            <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
                        </tr>
<?php endforeach; ?>
                </tbody>
            </table>
        </div>
        </div>
      </div>

    </div>
  </div>
</div>
{% endblock %}
