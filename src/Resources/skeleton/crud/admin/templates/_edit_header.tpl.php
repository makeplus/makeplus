<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
  <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-1">
      <div class="d-flex align-items-baseline mr-5">
        <h5 class="text-dark font-weight-bold my-2 mr-5"><?php echo $title ?> <i class="fas fa-chevron-right"></i> Editar</h5>
      </div>
    </div>
    <div class="d-flex align-items-center">
      <a class="btn btn-info btn-sm mr-1" href="{{ path('<?php echo $route_name ?>_list') }}">Volver al listado</a>
      {{ include('<?php echo $root_template_views ?>/_delete_form.html.twig') }}
    </div>
  </div>
</div>
