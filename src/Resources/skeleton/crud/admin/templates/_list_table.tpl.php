<div class="table-responsive">
  <table class="table table-hover table-striped table-responsive-sm mb-0">
    <thead class="thead-dark">
      <tr>
<?php foreach ($entity_fields as $field): ?>
        <th scope="col">
          {{ knp_pagination_sortable( objs, '<?php echo ucfirst($field['fieldName']) ?>', 'obj.<?php echo ($field['columnName']) ?>') }}
        </th>
<?php endforeach; ?>
      <th></th>
      </tr>
    </thead>
    <tbody>
  {% for <?= $entity_twig_var_singular ?> in objs %}
      <tr>
<?php foreach ($entity_fields as $field): ?>
<?php if ($field['fieldName'] == 'id'): ?>
        <td scope="row">
          <a class="font-weight-bold" href="{{ path('<?= $route_name ?>_show', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}">
            {{ <?php echo $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}
          </a>
        </td>
<?php elseif ( in_array($field['columnName'], array('created_at','updated_at')) ): ?>
        <td>{{ <?php echo $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> | date('d/m/Y H:i') }}</td>
<?php else: ?>
        <td>{{ <?php echo $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
<?php endif ?>
<?php endforeach; ?>
        <td>
          <a class="btn btn-edit btn-warning btn-sm py-1" href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}">Editar</a>
        </td>
      </tr>
    {% else %}
      <tr>
        <td colspan="<?= (count($entity_fields) + 1) ?>">
          <div class="row">
            <div class="col-12">
              <div class="alert alert-warning">
                Sin resultados
              </div>
            </div>
          </div>
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>