{% extends '_layout/layout.html.twig' %}

{% block title %}<?php echo ('Editar '.$title) ?>{% endblock %}

{% block body %}

{{ include('<?php echo $root_template_views ?>/_edit_header.html.twig', {}) }}

<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card">
      <div class="card-body">
        {{ include('_assets/flashMessages.html.twig', {}) }}
        <div class="row">
          <div class="col-12">
            {{ include('<?php echo $root_template_views ?>/_form_edit.html.twig', {'button_label': 'Guardar'}) }}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
{% endblock %}
