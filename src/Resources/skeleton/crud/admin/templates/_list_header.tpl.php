<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
  <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-1">
      <div class="d-flex align-items-baseline mr-5">
        <h5 class="text-dark font-weight-bold my-2 mr-5"><?php echo $title ?></h5>
          {% if form_filter.children|length > 1 %}
          <div class="subheader-separator subheader-separator-ver mr-5 bg-gray-200"></div>
          <a data-toggle="collapse" href="#collapseFilters" role="button" aria-expanded="false" aria-controls="collapseFilters" class="btn btn-light-primary font-weight-bolder btn-sm">Filtrar</a>
          {% endif %}
      </div>
    </div>
    <div class="d-flex align-items-center">
      <a class="btn btn-success btn-sm" href="{{ path('<?= $route_name ?>_new') }}">Nuevo</a>
    </div>
  </div>
</div>
