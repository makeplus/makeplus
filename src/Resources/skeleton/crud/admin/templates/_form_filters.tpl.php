{% form_theme form 'bootstrap_4_layout.html.twig' %}

{% if notCollapse is defined and notCollapse == true %}
	{% set cssClassCollapse = '' %}
{% else %}
	{% set cssClassCollapse = 'collapse' %}
{% endif %}

<div class="{{ cssClassCollapse }} {% if form_filter.vars['submitted'] %}show{% endif %}" id="collapseFilters">
	<div id="row-crud-filter" class="row">
		<div class="col-lg-12">
			<form id="filterFormReset" name="reset{{ (form.vars.name) }}" method="post">
				<input type="hidden" name="{{ (form.vars.name) }}[reset]" value="Limpiar">
			</form>
			{{ form_start(form) }}
				<div class="card-header bg-tertiary">
					<div class="card-title">
						<h3 class="card-label text-white">Filtros</h3>
					</div>
					<div class="card-toolbar">
						<a class="btn btn-sm btn-danger btn-reset mr-2" onclick="return document.forms['reset{{ (form.vars.name) }}'].submit();" href="#">Limpiar</a>
						<a class="btn btn-sm btn-success btn-filter "   onclick="return document.forms['{{ (form.vars.name) }}'].submit();" href="#">Filtrar</a>
					</div>
				</div>
				<div class="card-body">
					{{ form_widget(form, { 'id': 'filter-form-fields' }) }}
				</div>
			{{ form_end(form) }}
		</div>
	</div>
</div>