{% extends '_layout/layout.html.twig' %}

{% block title %}<?php echo ($title . ' Nuevo') ?>{% endblock %}

{% block body %}
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
  <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-1">
      <div class="d-flex align-items-baseline mr-5">
        <h5 class="text-dark font-weight-bold my-2 mr-5"><?php echo $title ?> <i class="fas fa-chevron-right"></i> Crear</h5>
      </div>
    </div>
    <div class="d-flex align-items-center">
      <a class="btn btn-danger btn-sm" href="{{ path('<?php echo $route_name ?>_list') }}">Cancelar</a>
    </div>
  </div>
</div>

<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card">
      <div class="card-body">
        {{ include('_assets/flashMessages.html.twig', {}) }}
        <div class="row">
          <div class="col-12">
            {{ include('<?php echo $root_template_views ?>/_form_create.html.twig', {'button_label': 'Crear'}) }}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
{% endblock %}
