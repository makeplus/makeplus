{% extends '_layout/layout.html.twig' %}

{% block title %}<?php echo $title; ?>{% endblock %}

{% block body %}

{{ include('<?php echo $root_template_views; ?>/_list_header.html.twig', { 'form_filter': form_filter }) }}

<div class="d-flex flex-column-fluid">
  <div class="container-fluid">
    <div class="card card-custom">
      <div class="card-body p-0">
        {{ include('_assets/flashMessages.html.twig', {}) }}
        {% if form_filter.children|length > 1 %}
            {{ include('<?php echo $root_template_views; ?>/_form_filters.html.twig', { 'form': form_filter }) }}
        {% endif %}
        <div id="row-crud-header" class="row">
        </div>
        <div id="row-crud-table" class="row">
          <div class="col-12">
            {{ include('<?php echo $root_template_views; ?>/_list_table.html.twig', { 'objs': objs }) }}
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div id="row-crud-foot" class="row">
          <div class="col-3">
            <div class="totales">Total: <strong>{{ objs.getTotalItemCount }}</strong></div>
          </div>
          <div class="col-9">
            {{ knp_pagination_render( objs, '_assets/paginator/pagination.html.twig') }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
