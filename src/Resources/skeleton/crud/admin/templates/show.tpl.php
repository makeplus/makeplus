{% extends '_layout/layout.html.twig' %}

{% block title %}<?php echo ('Vista '.$title .' #{{ '. $entity_twig_var_singular .'.'. $entity_identifier.' }}') ?>{% endblock %}

{% block body %}

{{ include('<?php echo $root_template_views ?>/_show_header.html.twig', { '<?= $entity_twig_var_singular ?>': <?= $entity_twig_var_singular ?> }) }}

<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card">
      <div class="card-body">
        {{ include('_assets/flashMessages.html.twig', {}) }}
        <div class="row">
          <div class="col-12">
            <table class="table">
                <tbody>
<?php foreach ($entity_fields as $field): ?>
                        <tr>
                            <th><?= ucfirst($field['fieldName']) ?></th>
                            <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
                        </tr>
<?php endforeach; ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}
