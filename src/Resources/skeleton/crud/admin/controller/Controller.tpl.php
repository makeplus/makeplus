<?php echo "<?php\n" ?>

namespace <?php echo $namespace ?>;

use <?php echo $entity_full_class_name ?>;
use <?php echo $repository_full_class_name ?>;

// Forms
use <?php echo $form_create_full_class_name ?>;
use <?php echo $form_edit_full_class_name ?>;
use <?php echo $form_filter_full_class_name ?>;

// sF and Doctrine
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// crud
use makeplus\makeplus\Controller\sfCrudAdminAbstractController;
// Filters
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
// Paginado
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("<?php echo $route_path ?>")
 */
class <?php echo $class_name ?> extends sfCrudAdminAbstractController<?php echo "\n" ?>
{
    public $templates_path = '<?php echo $templates_path ?>';

    public $route_prefix   = '<?php echo $route_name ?>';

    public $nameVarToTheme = '<?php echo $entity_twig_var_singular ?>';

    public $nameSessionFilter = '<?php echo $class_name ?>/list/filters';
    public $rows_per_page = 20;

    /**
     * @Route("/", name="<?php echo $route_name ?>_list", methods={"GET","POST"})
     */
    public function list(
        request $request , 
        FilterBuilderUpdaterInterface $filterBuilderUpdater,
        PaginatorInterface $paginator
    ): Response {

        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository(<?php echo $entity_class_name ?>::class)
            ->createQueryBuilder('obj')
            ->select('obj');



        $formFilter = $this->createForm(<?php echo $form_filter_class_name ?>::class);
        $this->changeFilters($request, $formFilter->getConfig()->getName());
        $formFilter = $this->applyFilters( $request, $formFilter );
        $filterBuilderUpdater->addFilterConditions($formFilter, $queryBuilder);

        $objs = $paginator->paginate( $queryBuilder, $request->query->getInt('page', 1), $this->rows_per_page );

        return $this->renderCrud('list.html.twig', [
            'objs' => $objs,
            'form_filter' => $formFilter->createView()
        ]);
    }

    /**
     * @Route("/new", name="<?php echo $route_name ?>_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $obj = new <?php echo $entity_class_name ?>();
        return $this->crudNew( $obj, <?php echo $form_create_class_name ?>::class, $request );
    }
/*
    public function saveNew($<?php echo $entity_twig_var_singular ?>) {




        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($<?php echo $entity_twig_var_singular ?>);
        $entityManager->flush();
    }
*/
    /**
     * @Route("/{<?php echo $entity_identifier ?>}/edit", name="<?php echo $route_name ?>_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, <?php echo $entity_class_name ?> $obj): Response {
        return $this->crudEdit( $obj, <?php echo $form_edit_class_name ?>::class, $request );
    }
/*
    public function saveEdit($<?php echo $entity_twig_var_singular ?>) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($<?php echo $entity_twig_var_singular ?>);
        $entityManager->flush();
    }
*/

    /**
     * @Route("/{<?php echo $entity_identifier ?>}/delete", name="<?php echo $route_name ?>_delete", methods={"DELETE"})
     */
    public function delete(Request $request, <?php echo $entity_class_name ?> $obj): Response
    {
        if ($this->isCsrfTokenValid('delete'.$obj->get<?php echo ucfirst($entity_identifier) ?>(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($obj);
            $entityManager->flush();
        }

        return $this->redirectToRoute( $this->route_prefix . 'list' );
    }

    /**
     * @Route("/{<?php echo $entity_identifier ?>}", name="<?php echo $route_name ?>_show", methods={"GET"})
     */
    public function show(<?php echo $entity_class_name ?> $obj): Response
    {
        return $this->renderCrud('show.html.twig', [
            '<?php echo $entity_twig_var_singular ?>' => $obj,
        ]);
    }
}
